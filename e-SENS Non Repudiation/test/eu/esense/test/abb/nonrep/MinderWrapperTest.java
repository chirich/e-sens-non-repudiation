package eu.esense.test.abb.nonrep;

import java.lang.reflect.Method;

import javax.xml.bind.DatatypeConverter;

import minderengine.ISignalHandler;
import minderengine.MinderUtils;

import org.junit.Before;
import org.junit.Test;

public class MinderWrapperTest {

  /**
   * Wrapper under test.
   */
  private MinderNonRepudiationWrapper wrapper;

  /**
   * A dummy signal handler that prints the arguments that it received. This is
   * only for test, normall the signals are handled automatically by
   * minder-client.
   * 
   * @author yerlibilgin
   * 
   */
  public class DummySignalHandler implements ISignalHandler {

    /**
     * For each argument, print its contents one tab indented.
     */
    @Override
    public Object handleSignal(Object obj, Method signalMethod, Object[] args) {
      for (Object object : args) {
        System.out.println("Arg:");
        if (object instanceof byte[]) {
          byte[] byt = (byte[]) object;
          System.out.println("\tHEX:");
          String str = DatatypeConverter.printHexBinary(byt);
          String []lns = str.split("\\n");
          for (String string : lns) {
            System.out.println("\t" + string);
          }

          System.out.println("\tString:");
          str = new String(byt);
          lns = str.split("\\n");
          for (String string : lns) {
            System.out.println("\t" + string);
          }
        } else {
          System.out.println("\t" + object);
        }
        System.out.println("-----------------");
      }
      return null;
    }
  }

  @Before
  public void before() {
    //can't initialize the wrapper directly. it is abstract. let minder do it for us.
    wrapper = (MinderNonRepudiationWrapper) MinderUtils.createWrapper(MinderNonRepudiationWrapper.class,
        new DummySignalHandler());
    
    //start our SUT.
    wrapper.start();
  }
  
  @Test
  public void testAtna(){
    //we are behaving as minder now, call the slot.
    wrapper.handleIncomingMessage();
  }
}
