package eu.esens.abb.nonrep;

import java.util.HashMap;
import java.util.List;

import org.herasaf.xacml.core.policy.impl.AttributeAssignmentType;
import org.w3c.dom.Document;


public class ATNAObligationHandler implements ObligationHandler {

	private IHEMessageType messageType;
	private List<ESensObligationHandler> obligations;
	private final static String ATNA_PREFIX = "urn:eSENS:obligations:nrr:ATNA";
	private final HashMap<String, String> auditValueMap = new HashMap<String, String>();
	private Document audit = null;
	
	public ATNAObligationHandler(MessageType messageType,
			List<ESensObligationHandler> obligations) {
		this.messageType = (IHEMessageType)messageType;
		this.obligations = obligations;
	}

	/**
	 *  Discharge returns the object discharged, or exception(non-Javadoc)
	 * @throws ObligationDischargeException 
	 * @see eu.esens.abb.nonrep.ObligationHandler#discharge()
	 */
	
	@Override
	public void discharge() throws ObligationDischargeException {
		/*
		 * Here I need to check the IHE message type. It can be XCA, XCF, whatever
		 */

		if ( messageType instanceof IHEXCARetrieve ) {
			makeIHEXCARetrieveAudit((IHEXCARetrieve)messageType, obligations);
		} else {
			throw new ObligationDischargeException("Unkwnon message type");
		}
	}

	private void makeIHEXCARetrieveAudit(IHEXCARetrieve messageType2,
			List<ESensObligationHandler> obligations2) {
		int size = obligations2.size();
		
		for ( int i=0; i<size; i++ ) {
			ESensObligationHandler eSensOblHandler = obligations2.get(i);
			
			String outcome = null;
			if ( eSensOblHandler instanceof PERMITEsensObligationHandler ) {
				outcome = "SUCCESS";
			}
			else {
				outcome = "MINOR FAILURE";
			}
			
			// Here the real mapping happens: are we NRR or NRO? I think both, and it depends on the policy
			List<AttributeAssignmentType> nroAttributes = eSensOblHandler.getNROAttributes();
			
			if ( nroAttributes != null ) {
				int attributeSize = nroAttributes.size();
				
				for ( int j = 0; j< attributeSize; j++ ) {
					AttributeAssignmentType aat = nroAttributes.get(j);
					
					if ( aat.getAttributeId().startsWith(ATNA_PREFIX) ) {
						fillHash(aat.getAttributeId(), aat.getContent());
					}
				}
			}
			List<AttributeAssignmentType> nrrAttributes = eSensOblHandler.getNRRAttributes();

			if ( nrrAttributes != null ) {
				int attributeSize = nrrAttributes.size();
				
				for ( int j = 0; j< attributeSize; j++ ) {
					AttributeAssignmentType aat = nrrAttributes.get(j);
					
					if ( aat.getAttributeId().startsWith(ATNA_PREFIX) ) {
						fillHash(aat.getAttributeId(), aat.getContent());
					}
				}
			}
			
			makeAuditXml(outcome);
		}
		
	}

	private void makeAuditXml(String outcome) {
		// CALL OPENATNA! :)
		
		/*
		 * Read the values that you need from the auditValueMap and fill the XML
		 */
	}

	private void fillHash(String attributeId, List<Object> content) {
		System.out.println(attributeId.substring(ATNA_PREFIX.length()+1)); // the colon
		System.out.println(content.get(0).getClass());
		auditValueMap.put(attributeId.substring(ATNA_PREFIX.length()+1), (String)content.get(0));
		
	}

	@Override
	public Document getMessage() {
		return audit;
	}

}
