package eu.esense.test.abb.nonrep;

import java.io.ByteArrayOutputStream;

import minderengine.Signal;
import minderengine.Slot;
import minderengine.Wrapper;

import org.w3c.dom.Document;

import eu.esens.abb.nonrep.Utilities;

/**
 * This class is the bridge between "Minder test engine" and the system under
 * test. It performs a remote communication with the test server and joins the
 * game as a wrapper. (minder is turkish for "cushion" which is inspired from
 * the "bed" part of a test :-)
 * 
 * It is abstract (because of abstarct signals), we instantiate it in the lower
 * level using CGI.
 * 
 * @author yerlibilgin
 * 
 */
public abstract class MinderNonRepudiationWrapper extends Wrapper {

  private boolean inited = false;

  /**
   * Called by the server when a test case that contains this wrapper is about
   * to be run. Perform any initialization here
   */
  @Override
  public void start() {
    if (inited)
      return;
    try {
      EvidenceEmitterTest.setUpBeforeClass();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }

    inited = true;
  }

  /**
   * Called by the server when a test case that conatins this wrapper is
   * finished. Perform your own resource deallocation here.
   */
  @Override
  public void stop() {
  }

  /**
   * The display name that this wrapper will be shown as in the minder test
   * engine reports. if there are multiple wrappers with the same name an
   * integer suffix will be added to this label by the server.
   */
  @Override
  public String getLabel() {
    // the default label is "Wrapper". Lets give a more related name (give
    // whatever you want)
    return "NonRepudiation";
  }

  /**
   * This slot will be called by minder when the test starts. Note that we
   * didn't declare any parameters as inputData.xml because the sample xml
   * already exists on the client side, thus we don't need (for now) to send the
   * sample data from the server.
   * 
   * later on we can change the method to be: <code>
   *  public void handleIncomingMessage(byte []inputData) {
   *  ...
   *  </code>
   * 
   * This method is void, because we "suppose that we don't know when the
   * evidence builder will be ready for providing the evidence" (still
   * generically speaking). Thus we will provide the evidence later via a
   * signal. (In the EvidenceEmitterTest the evidence is actaully built
   * immediately so we can still return it from here, but we want to make sure
   * that we are ready for the future and also it will be good to obey minder
   * test engine pattern.).
   */
  @Slot
  public void handleIncomingMessage() {
    EvidenceEmitterTest eet = new EvidenceEmitterTest();
    try {
      // perform test and handle the atna evidence.
      // we should be generic later when we are ready for ETSI REM stuff.
      Document atna = eet.testGenerateAtna();
      if (atna == null)
        throw new IllegalStateException("Document had to be available here");

      // serialize the document to a byte array and signal it to minder.
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      Utilities.serialize(atna.getDocumentElement(), baos);

      // signal
      evidenceGenerated(baos.toByteArray());
    } catch (Exception ex) {
      // we have a problem. send a runtime exception back to server
      if (ex instanceof RuntimeException)
        throw (RuntimeException) ex;

      throw new RuntimeException(ex);
    }
  }

  @Signal
  public abstract void evidenceGenerated(byte[] byteArray);
}
