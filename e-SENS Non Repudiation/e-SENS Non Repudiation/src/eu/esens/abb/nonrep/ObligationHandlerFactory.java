package eu.esens.abb.nonrep;

import java.util.List;

import org.apache.log4j.Logger;

public class ObligationHandlerFactory {

	private static ObligationHandlerFactory instance = null;
	private static Logger l = Logger.getLogger(ObligationHandlerFactory.class);
	
	public static ObligationHandlerFactory getInstance() {
		if (instance == null) {
			synchronized (ObligationHandlerFactory.class) {
				if (instance == null) {
					try {
						instance = new ObligationHandlerFactory();
					} catch (Exception e) {
						e.printStackTrace();

						throw new IllegalStateException(
								"Unable to instantiate the ObligationHandlerFactory: "
										+ e.getMessage(), e);
					}
				}
			}
		}
		return instance;
	}

	private ObligationHandlerFactory() {
		l.debug("In the ObligationHandlerFactory constructor");
	}

	/**
	 * Dispatch the correct obligation handler based, on the family of message types
	 * @param messageType
	 * @param obligations
	 * @return
	 * @throws ObligationDischargeException 
	 */
	public ObligationHandler createHandler(MessageType messageType,
			List<ESensObligationHandler> obligations) throws ObligationDischargeException {
		
		if ( messageType == null ) {
			throw new ObligationDischargeException("Message Type is null");
		}
		

		if ( messageType instanceof IHEMessageType ) {
			return new ATNAObligationHandler(messageType, obligations);
		} else {
			throw new ObligationDischargeException("Unknown message type");
		}
	}
	

}
