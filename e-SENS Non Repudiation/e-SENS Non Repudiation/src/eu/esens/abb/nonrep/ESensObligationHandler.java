package eu.esens.abb.nonrep;

import java.util.List;

import org.herasaf.xacml.core.policy.impl.AttributeAssignmentType;

/**
 * The four methods will be removed if there is no need (e.g., we don't need to 
 * pack a single NR token)
 * @author max
 *
 */
public class ESensObligationHandler {

	private List<AttributeAssignmentType> nro;
	private List<AttributeAssignmentType> nrr;
	private List<AttributeAssignmentType> nrd;
	private List<AttributeAssignmentType> nrs;
	private List<AttributeAssignmentType> unknown;

	public void setNROToken(List<AttributeAssignmentType> attrAssignments) {
		this.nro = attrAssignments;
	}

	public void setNRRToken(List<AttributeAssignmentType> attrAssignments) {
		this.nrr = attrAssignments;
	}

	public void setNRDToken(List<AttributeAssignmentType> attrAssignments) {
		this.nrd = attrAssignments;
	}

	public void setNRSToken(List<AttributeAssignmentType> attrAssignments) {
		this.nrs = attrAssignments;
	}

	public void setUnknownToken(List<AttributeAssignmentType> attrAssignments) {
		this.unknown = attrAssignments;
	}

	public final List<AttributeAssignmentType> getNROAttributes() {
		return nro;
	}

	public final List<AttributeAssignmentType> getNRRAttributes() {
		return nrr;
	}

	public final List<AttributeAssignmentType> getNRDAttributes() {
		return nrd;
	}

	public final List<AttributeAssignmentType> getNRSAttributes() {
		return nrs;
	}

	public final List<AttributeAssignmentType> getUnknownAttributes() {
		return unknown;
	}
	


}
